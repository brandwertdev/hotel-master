<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>

<div id="outermain-wrapper" class="inner">
    <div id="outermain">
        <div id="maincontainer">
            <div id="maincontent-container">
                <div class="container">
                    <div class="row">
                        <section id="maincontent" class="hassidebar mborderright">

                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <h3>Custom Fields</h3>

                                <p>
                                    events_date = <?php the_field('events_date'); ?><br>
                                    events_capacity = <?php the_field('events_capacity'); ?><br>
                                </p>
                            </article>

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>