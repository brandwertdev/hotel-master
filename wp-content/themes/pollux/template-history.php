<?php
/**
 * Template Name: Geschichte
 * Description: Anzeige der chronologischen Geschichte
 */
?>

<?php $query = new WP_Query(array('post_type' => 'history', 'posts_per_page' => 11)); ?>

<?php get_header(); ?>

    <div id="outermain-wrapper" class="inner">
        <div id="outermain">
            <div id="maincontainer">
                <div id="maincontent-container">
                    <div class="container">
                        <div class="row">
                            <section id="maincontent" class="hassidebar mborderright">
                                <?php while ($query->have_posts()) : $query->the_post(); ?>

                                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                        <header class="entry-header">
                                            <?php if (has_post_thumbnail() && !post_password_required()) : ?>
                                                <div class="entry-thumbnail">
                                                    <?php the_post_thumbnail(); ?>
                                                </div>
                                            <?php endif; ?>

                                            <h1 class="entry-title"><a href="<?php the_permalink(); ?>"
                                                                       rel="bookmark"><?php the_title(); ?></a></h1>
                                        </header>
                                        <!-- .entry-header -->

                                        <div class="entry-content">
                                            <?php the_content(); ?>
                                            <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentythirteen') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
                                            <h3>Custom Fields</h3>

                                            <p>
                                                time_line_date = <?php the_field('time_line_date'); ?><br>
                                            </p>
                                        </div>
                                        <!-- .entry-content -->


                                    </article><!-- #post -->

                                <?php endwhile; ?>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>