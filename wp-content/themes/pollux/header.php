<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pollux Theme</title>


    <?php
        //ADD HEADER LIBRARIES / ENQUEUE
        wp_head()
    ?>
    <style type="text/css" media="screen"> .simple-social-icons ul li a, .simple-social-icons ul li a:hover { background-color: #f1f1f1 !important; border-radius: 3px; color: #564152 !important; border: 0px #ffffff solid !important; font-size: 18px; padding: 9px; }  .simple-social-icons ul li a:hover { background-color: #f1f1f1 !important; border-color: #ffffff !important; color: #f48147 !important; }</style>
</head>

<?php
    // CHECK IF FRONT PAGE
    if( is_front_page() ):
        $ht99_classes = array( 'pollux', 'is-home', 'klasikt' );
    else:
        $ht99_classes = array( 'pollux', 'not-home', 'klasikt' );
    endif;
?>

<body <?php body_class( $ht99_classes ); ?>>
    <div id="bodychild">
        <div id="outercontainer">
             <!-- HEADER -->
            <div id="outerheader">
                <div id="headercontainer">
                    <header id="top">

                        <div id="outermainmenu">
                            <div class="container">
                                <div class="row">
                                    <section id="navigation" class="twelve columns">
                                        <?php
                                            // INCLUDE PRIMARY MENU

                                            wp_nav_menu(array(
                                                'theme_location' => 'primary',
                                                'menu_class' => 'sf-menu sf-js-enabled sf-arrows l_tinynav1',
                                                'menu_id' => 'topnav',
                                                'container_id' => 'nav-wrap'
                                            ));
                                        ?>
                                    </section>
                                </div>
                            </div>
                        </div>

                        <div id="outerlogo">
                            <div class="container">
                                <div class="row">
                                    <div id="logo" class="twelve columns">
                                        <div id="logoimg">
                                            <a href="http://demo.klasikthemes.com/imperial/" title="Imperial">
                                                <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="">
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>

                    <div id="bg-image">
                        <img src="<?php echo get_template_directory_uri() ?>/images/bg-logo-inner.jpg" alt="">
                    </div>

                    <div id="outerafterheader">
                        <div class="container">
                            <div class="row">

                                <div id="afterheader" class="twelve columns">
                                    <h1 class="pagetitle nodesc"><?php wp_title() ?></h1>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



            </div>

