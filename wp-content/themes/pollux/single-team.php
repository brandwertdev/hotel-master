<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>

<div id="outermain-wrapper" class="inner">
    <div id="outermain">
        <div id="maincontainer">
            <div id="maincontent-container">
                <div class="container">
                    <div class="row">
                        <section id="maincontent" class="hassidebar mborderright">


                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <h3>Custom Fields</h3>

                                <p>
                                    staff_title = <?php the_field('staff_title'); ?><br>
                                    staff_email = <?php the_field('staff_email'); ?><br>
                                    staff_sykpe = <?php the_field('staff_sykpe'); ?><br>
                                    staff_facebook = <?php the_field('staff_facebook'); ?><br>
                                    staff_twitter = <?php the_field('staff_twitter'); ?><br>
                                    staff_google_plus = <?php the_field('staff_google_plus'); ?><br>
                                </p>

                            </article>

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>