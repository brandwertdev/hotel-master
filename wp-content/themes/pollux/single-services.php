<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>

<div id="outermain-wrapper" class="inner">
    <div id="outermain">
        <div id="maincontainer">
            <div id="maincontent-container">
                <div class="container">
                    <div class="row">
                        <section id="maincontent" class="hassidebar mborderright">


                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


                                <h3>Custom Fields</h3>

                                <p>
                                    services_price = <?php the_field('services_price'); ?><br>
                                    services_icon = <?php the_field('services_icon'); ?><br>
                                    services_in_slider = <?php the_field('services_in_slider'); ?><br>
                                </p>


                            </article>
                            <!-- #post -->

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>