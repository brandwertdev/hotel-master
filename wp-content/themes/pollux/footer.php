        <div id="footersection">
            <div id="outerfootersidebar">
                <div id="footersidebarcontainer">
                    <div class="container">
                        <div class="row">

                            <footer id="footersidebar">
                                <?php dynamic_sidebar('footer-widget-area-1'); ?>
                                <?php dynamic_sidebar('footer-widget-area-2'); ?>
                                <?php dynamic_sidebar('footer-widget-area-3'); ?>
                                <?php dynamic_sidebar('footer-widget-area-4'); ?>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>

            <div id="outerfooter">
                <div id="footercontainer">
                    <div class="container">
                        <div class="row">

                            <div class="twelve columns">
                                <footer id="footer">
                                    <div class="copyrighttext">
                                        Copyright © 2016 <a href="http://demo.klasikthemes.com/imperial/">Imperial</a>. Designed
                                        by <a href="http://www.klasikthemes.com" title="">Klasik Themes</a>.

                                    </div>
                                    <div class="footertext">
                                    </div>
                                </footer>
                                <div id="toTop"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>