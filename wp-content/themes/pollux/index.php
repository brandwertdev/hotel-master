<?php get_header(); ?>

<div id="outermain-wrapper" class="inner">
    <div id="outermain">
        <div id="maincontainer">
            <div id="maincontent-container">
                <div class="container">
                    <div class="row">
                        <section id="maincontent" class="hassidebar mborderright">
                            <?php if (have_posts()):

                                    while (have_posts()): the_post(); ?>

                                        <h1><?php the_title(); ?></h1>
                                        <p><?php the_content(); ?></p>

                                    <?php endwhile;
                                endif;
                            ?>

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>