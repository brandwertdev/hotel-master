<?php
/**
 * 99hotel Templates functions and definitions
 *
 */

function ht99_script_enqueue(){
    //INCLUDE STYLES
    wp_enqueue_style('skeleton', get_template_directory_uri() . '/css/skeleton.css');
    wp_enqueue_style('general', get_template_directory_uri() . '/css/general.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/css/template.css');
    wp_enqueue_style('color', get_template_directory_uri() . '/css/color.css');
    wp_enqueue_style('layout', get_template_directory_uri() . '/css/layout.css');
    wp_enqueue_style('simple-social-icons-font', get_template_directory_uri() . '/css/social-icons.css');


    //INCLUDE JAVASCRIPTS IN FOOTER
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/template.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'ht99_script_enqueue');


function ht99_theme_setup(){
    //Activate THEME SUPPORT
    add_theme_support('menus');

    register_nav_menu('primary', 'Primary Header Navigation');
    register_nav_menu('secondary', 'Footer Navigation');

}
add_action('init', 'ht99_theme_setup');


//Sidebar
function ht99_widget_area_setup(){
    register_sidebar(array(
        'name' => 'Footer 1',
        'id' => 'footer-widget-area-1',
        'class' => 'sepp',
        'description' => 'Footer Widget',
        'before_widget' => '<div id="footcol1" class="three columns"><div class="widget-area"><div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li></ul></div></div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer 2',
        'id' => 'footer-widget-area-2',
        'class' => '',
        'description' => 'Footer Widget',
        'before_widget' => '<div id="footcol2" class="three columns"><div class="widget-area"><div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li></ul></div></div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer 3',
        'id' => 'footer-widget-area-3',
        'class' => '',
        'description' => 'Footer Widget',
        'before_widget' => '<div id="footcol3" class="three columns"><div class="widget-area"><div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li></ul></div></div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Footer Sozial Icons',
        'id' => 'footer-widget-area-4',
        'class' => '',
        'description' => 'Footer Sozial Icons Widget',
        'before_widget' => '<div id="footcol2" class="three columns"><div class="widget-area"><div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s simple-social-icons">',
        'after_widget' => '</li></ul></div></div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}
add_action( 'widgets_init', 'ht99_widget_area_setup' );
