<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>



<header class="archive-header" style="background-image:url('<?php echo ht_category_banner(get_the_ID()); ?>')">
    <h1 class="archive-title">
        <?php the_title()?>
    </h1>
</header><!-- .archive-header -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


            <h3>Custom Fields</h3>
            <p>
                services_price = <?php the_field('services_price'); ?><br>
                services_icon = <?php the_field('services_icon'); ?><br>
                services_in_slider = <?php the_field('services_in_slider'); ?><br>
            </p>

        </div><!-- .entry-content -->

</article><!-- #post -->


<?php get_footer(); ?>