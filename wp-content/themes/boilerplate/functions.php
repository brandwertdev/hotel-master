<?php
/**
 * 99hotel Templates functions and definitions
 *
 */

function ht99_script_enqueue(){
    //INCLUDE STYLES
    wp_enqueue_style('default-style', get_stylesheet_uri() );
    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/template.css');

    //INCLUDE JAVASCRIPTS IN FOOTER
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/template.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'ht99_script_enqueue');


function ht99_theme_setup(){
    //Activate THEME SUPPORT
    add_theme_support('menus');
    add_theme_support( 'post-thumbnails' );

    //REGISTER THE MENUS
    register_nav_menu('primary', 'Primary Header Navigation');
    register_nav_menu('footer', 'Footer Navigation');

}
add_action('init', 'ht99_theme_setup');

