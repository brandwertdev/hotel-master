        <footer>
            <p>
                <?php wp_nav_menu(array(
                    'theme_location' => 'footer'
                ));
                ?>
            </p>
        </footer>

        <?php
            //ADD FOOTER LIBRARIES / ENQUEUES IN FOOTERS
            wp_footer();
        ?>

    </body>
</html>