<?php
/**
 * Template Name: Zimmer
 * Description: Übersicht aller Zimmer
 */
?>

<?php $query = new WP_Query( array('post_type' => 'rooms', 'posts_per_page' => 10 ) ); ?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

						<h3>Custom Fields</h3>
						<p>
							rooms_price = <?php the_field('rooms_price'); ?><br>
							rooms_min_stay = <?php the_field('rooms_min_stay'); ?><br>
							rooms_max_guest = <?php the_field('rooms_max_guest'); ?><br>
							rooms_count = <?php the_field('rooms_count'); ?><br>
							rooms_free_services_checkboxes = <?php the_field('rooms_free_services_checkboxes'); ?><br>
							rooms_paid_services_checkboxes = <?php the_field('rooms_paid_services_checkboxes'); ?><br>
							rooms_slideshow_images =
							<?php
							$values = get_field('rooms_images');
							print_console($values);
							if($values)
							{
								echo '<ul>';
								foreach($values as $value)
								{
									echo '<li>ID: ' . $value['ID'] . ' filename: ' . $value['filename'] . '</li>';
								}
								echo '</ul>';
							}
							?>
						</p>
					</div><!-- .entry-content -->

				</article><!-- #post -->

				<?php comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>