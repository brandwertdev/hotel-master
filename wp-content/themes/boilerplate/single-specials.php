<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>


<header class="archive-header" style="background-image:url('<?php echo ht_category_banner(get_the_ID()); ?>')">
    <h1 class="archive-title">
        <?php the_title()?>
    </h1>
</header>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <h3>Custom Fields</h3>
    <p>
        specials_price = <?php the_field('specials_price'); ?><br>
        specials_date_from = <?php the_field('specials_date_from'); ?><br>
        specials_date_until = <?php the_field('specials_date_until'); ?><br>
    </p>

</article>


<?php get_footer(); ?>