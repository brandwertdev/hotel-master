<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>Boilerplate Theme</title>


            <?php
                //ADD HEADER LIBRARIES / ENQUEUE
                wp_head()
            ?>
        </head>

        <?php
            // CHECK IF FRONT PAGE
            if( is_front_page() ):
                $ht99_classes = array( 'jungfrau', 'is-home' );
            else:
                $ht99_classes = array( 'jungfrau', 'not-home' );
            endif;
        ?>

        <body <?php body_class( $ht99_classes ); ?>>


        <?php
            // INCLUDE PRIMARY MENU
            wp_nav_menu(array(
                'theme_location' => 'primary'
            ));
        ?>