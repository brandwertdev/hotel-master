            <footer class="footer-wrapper">
                <div class="footer-container container">
                    <div class="footer-column three columns" id="footer-widget-1">
                        <div id="text-5" class="widget widget_text gdlr-item gdlr-widget"><h3 class="gdlr-widget-title">Jetzt Buchen</h3><div class="clear"></div>			<div class="textwidget"><p><i class="gdlr-icon fa fa-phone" style="color: #333; font-size: 16px; "></i> 044 123 45 67</p>
                                <div class="clear"></div>
                                <div class="gdlr-space" style="margin-top: -15px;"></div>
                                <p><i class="gdlr-icon fa fa-envelope-o" style="color: #333; font-size: 16px; "></i> buchen@99hotels.ch</p>
                                <div class="clear"></div>
                                <div class="gdlr-space" style="margin-top: 25px;"></div>
                                <p><a href="https://www.facebook.com/99hotels/" target="_blank"><i class="gdlr-icon fa fa-facebook-square" style="color: #333333; font-size: 24px; "></i></a> <a href="http://www.99hotels.ch" target="_blank"><i class="gdlr-icon fa fa-twitter-square" style="color: #333333; font-size: 24px; "></i></a> <a href="http://www.99hotels.ch" target="_blank"><i class="gdlr-icon fa fa-linkedin-square" style="color: #333333; font-size: 24px; "></i></a> <a href="http://www.99hotels.ch" target="_blank"><i class="gdlr-icon fa fa-google-plus-square" style="color: #333333; font-size: 24px; "></i></a> </p>
                            </div>
                        </div>				</div>
                    <div class="footer-column three columns" id="footer-widget-2">
                        <div id="recent-posts-5" class="widget widget_recent_entries gdlr-item gdlr-widget">		<h3 class="gdlr-widget-title">Letzte News</h3><div class="clear"></div>		<ul>
                                <li>
                                    <a href="http://www.99hotels.ch/demo/jungfrau/magna-pars-studiorum/">Unser Weihnachtsspecial</a>
                                </li>
                                <li>
                                    <a href="http://www.99hotels.ch/demo/jungfrau/sedial-eiusmod-tempor/">Spa Bereich</a>
                                </li>
                                <li>
                                    <a href="http://www.99hotels.ch/demo/jungfrau/gallery-post-format-title/">Neue Fotos</a>
                                </li>
                                <li>
                                    <a href="http://www.99hotels.ch/demo/jungfrau/audio-post-format/">Menukarte erweitert</a>
                                </li>
                                <li>
                                    <a href="http://www.99hotels.ch/demo/jungfrau/nihilne-te-nocturnum/">Webseite Redesign</a>
                                </li>
                                <li>
                                    <a href="http://www.99hotels.ch/demo/jungfrau/aside-post-format/">Montag geschlossen</a>
                                </li>
                            </ul>
                        </div>						</div>
                    <div class="footer-column six columns" id="footer-widget-3">
                        <div id="text-10" class="widget widget_text gdlr-item gdlr-widget"><h3 class="gdlr-widget-title">Unsere Auszeichnungen</h3><div class="clear"></div>			<div class="textwidget"><img src="http://www.99hotels.ch/demo/jungfrau/wp-content/uploads/2016/02/awards-logo-1.png" alt="">
                                <div class="clear"></div><div class="gdlr-space" style="margin-top: 20px;"></div>
                                Überzeugen Sie sich selber von unseren Dienstleistungen und buchen Sie noch heute!</div>
                        </div>				</div>
                    <div class="clear"></div>
                </div>

                <div class="copyright-wrapper">
                    <div class="copyright-container container">
                        <div class="copyright-left">
                            <?php wp_nav_menu(array(
                                'theme_location' => 'footer'
                            ));
                            ?>
                        </div>
                            <div class="copyright-right">Copyright 2016 Alle Rechte vorbehalten</div>
                            <div class="clear"></div>
                    </div>
                </div>
            </footer>


            <?php
                //ADD FOOTER LIBRARIES / ENQUEUES IN FOOTERS
                wp_footer();
            ?>
        </div>
    </body>
</html>