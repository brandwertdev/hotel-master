<?php
/**
 * 99hotel Templates functions and definitions
 *
 */

include_once( 'include/gdlr-navigation-menu.php');


function ht99_script_enqueue(){
    //INCLUDE STYLES
    wp_enqueue_style( 'Open-Sans-google-font-css', 'http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;subset=greek%2Ccyrillic-ext%2Ccyrillic%2Clatin%2Clatin-ext%2Cvietnamese%2Cgreek-ext&#038;ver=4.4.2', false );
    wp_enqueue_style( 'Merriweather-google-font-css', 'http://fonts.googleapis.com/css?family=Merriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&subset=latin%2Clatin-ext&ver=4.4.2', false );


    wp_enqueue_style('swiper-css', get_template_directory_uri() . '/css/swiper/swiper.min.css');
    wp_enqueue_style('gdlr-hotel-css', get_template_directory_uri() . '/css/gdlr-hotel.css');
    wp_enqueue_style('default-style', get_stylesheet_uri() );
    wp_enqueue_style('superfish-css', get_template_directory_uri() . '/css/superfish/superfish.css');
    wp_enqueue_style('dlmenu-css', get_template_directory_uri() . '/css/dl-menu/component.css');
    wp_enqueue_style('flexslider-css', get_template_directory_uri() . '/css/flexslider/flexslider.css');
    wp_enqueue_style('style-responsive-css', get_template_directory_uri() . '/css/style-responsive.css');
    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/style-custom.css');

    wp_enqueue_style('template-css', get_template_directory_uri() . '/css/template.css');

    //INCLUDE JAVASCRIPTS IN FOOTER
    wp_enqueue_script("jquery");
    wp_enqueue_script("jquery-migrate");
    wp_enqueue_script('superfish-js', get_template_directory_uri() . '/js/superfish/superfish.js', array(), '1.0.0', true);
    wp_enqueue_script('superfish-hoverIntend-js', get_template_directory_uri() . '/js/superfish/hoverIntent.js', array(), '1.0.0', true);
    wp_enqueue_script('dlmenu-modernizr', get_template_directory_uri() . '/js/dl-menu/modernizr.custom.js', array(), '1.0.0', true);
    wp_enqueue_script('dlmenu-jquery-dlmenu', get_template_directory_uri() . '/js/dl-menu/jquery.dlmenu.js', array(), '1.0.0', true);
    wp_enqueue_script('jquery-easing', get_template_directory_uri() . '/js/jquery.easing.js', array(), '1.0.0', true);
    wp_enqueue_script('jquery-transit', get_template_directory_uri() . '/js/jquery.transit.min.js', array(), '1.0.0', true);
    wp_enqueue_script('flexslider-js', get_template_directory_uri() . '/js/flexslider/jquery.flexslider.js', array(), '1.0.0', true);
    wp_enqueue_script('jquery-isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array(), '1.0.0', true);
    wp_enqueue_script('jquery-swiper-js', get_template_directory_uri() . '/js/swiper/swiper.min.js', array(), '1.0.0', true);
    wp_enqueue_script('gdlr-script-js', get_template_directory_uri() . '/js/gdlr-script.js', array(), '1.0.0', true);

    wp_enqueue_script('template-js', get_template_directory_uri() . '/js/template.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'ht99_script_enqueue');


function ht99_theme_setup(){
    //Activate THEME SUPPORT
    add_theme_support('menus');

    add_theme_support( 'post-thumbnails' );
    add_image_size( 'banner', 1220, 580 );

    //REGISTER THE MENUS
    register_nav_menu('primary', 'Primary Header Navigation');
    register_nav_menu('footer', 'Footer Navigation');

}
add_action('init', 'ht99_theme_setup');

