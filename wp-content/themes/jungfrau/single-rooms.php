<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>



<header class="archive-header" style="background-image:url('<?php echo ht_category_banner(get_the_ID()); ?>')">
    <h1 class="archive-title">
        <?php the_title()?>
    </h1>
</header><!-- .archive-header -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <h3>Custom Fields</h3>
    <p>
        rooms_price = <?php the_field('rooms_price'); ?><br>
        rooms_min_stay = <?php the_field('rooms_min_stay'); ?><br>
        rooms_max_guest = <?php the_field('rooms_max_guest'); ?><br>
        rooms_count = <?php the_field('rooms_count'); ?><br>
        rooms_free_services_checkboxes = <?php the_field('rooms_free_services_checkboxes'); ?><br>
        rooms_paid_services_checkboxes = <?php the_field('rooms_paid_services_checkboxes'); ?><br>
        rooms_slideshow_images =
        <?php
            $values = get_field('rooms_images');
            print_console($values);
            if($values)
            {
                echo '<ul>';
                foreach($values as $value)
                {
                    echo '<li>ID: ' . $value['ID'] . ' filename: ' . $value['filename'] . '</li>';
                    echo '<li><img src="' . $value['sizes']['thumbnail'] .'" alt="" /></li>';
                }
                echo '</ul>';
            }
        ?>
    </p>
</article>

<?php get_footer(); ?>