<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>Jungfrau Master Theme</title>


            <?php
                //ADD HEADER LIBRARIES / ENQUEUE
                wp_head()
            ?>
        </head>

        <?php
            // CHECK IF FRONT PAGE
            if( is_front_page() ):
                $ht99_classes = array( 'jungfrau', 'is-home' );
            else:
                $ht99_classes = array( 'jungfrau', 'not-home' );
            endif;
        ?>

        <body <?php body_class( $ht99_classes ); ?>>
            <div class="body-wrapper gdlr-boxed-style float-menu gdlr-icon-dark gdlr-header-solid">
                <header class="gdlr-header-wrapper">
                    <!-- top navigation -->
                    <div class="top-navigation-wrapper">
                        <div class="top-navigation-container container">
                            <div class="top-navigation-left">
                                <div class="top-navigation-left-text">
                                    Limmatstrasse 99, 8000 Zürich					</div>
                            </div>
                            <div class="top-navigation-right">
                                <div class="top-social-wrapper">
                                    <div class="social-icon">
                                        <a href="#" target="_blank">
                                            <img width="32" height="32" src="http://www.99hotels.ch/demo/jungfrau/wp-content/themes/hotelmaster-v2-05/images/light/social-icon/facebook.png" alt="Facebook">
                                        </a>
                                    </div>
                                    <div class="social-icon">
                                        <a href="#" target="_blank">
                                            <img width="32" height="32" src="http://www.99hotels.ch/demo/jungfrau/wp-content/themes/hotelmaster-v2-05/images/light/social-icon/google-plus.png" alt="Google Plus">
                                        </a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="top-navigation-divider"></div>

                    <!-- logo / navigation -->
                    <div class="gdlr-header-inner">
                        <div class="gdlr-header-container container">
                            <!-- logo -->
                            <div class="gdlr-logo">
                                <div class="gdlr-logo-inner">
                                    <a href="http://www.99hotels.ch/demo/jungfrau">
                                        <img src="http://www.99hotels.ch/demo/jungfrau/wp-content/uploads/2016/02/logo.png" alt="" width="273" height="59">						</a>
                                    <div class="gdlr-responsive-navigation dl-menuwrapper" id="gdlr-responsive-navigation"><button class="dl-trigger">Open Menu</button><ul id="menu-hauptmenu-jungfrau" class="dl-menu gdlr-main-mobile-menu"><li id="menu-item-4020" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4020"><a href="http://www.99hotels.ch/demo/jungfrau/">Home</a></li>
                                            <li id="menu-item-4021" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4021"><a href="http://www.99hotels.ch/demo/jungfrau/_jungfrau/zimmer/">Zimmer</a></li>
                                            <li id="menu-item-4117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4117"><a href="http://www.99hotels.ch/demo/jungfrau/bildergalerie/">Bildergalerie</a></li>
                                            <li id="menu-item-4039" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4039"><a href="http://www.99hotels.ch/demo/jungfrau/_jungfrau/kontakt/">Kontakt</a>
                                                <ul class="dl-submenu"><li class="dl-back"><a href="#">back</a></li><li class="menu-item gdlr-parent-menu"><a href="http://www.99hotels.ch/demo/jungfrau/_jungfrau/kontakt/">Kontakt</a></li>
                                                    <li id="menu-item-4126" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4126"><a href="http://www.99hotels.ch/demo/jungfrau/_jungfrau/team/">Team</a></li>
                                                </ul>
                                            </li>
                                        </ul></div>					</div>
                            </div>

                            <!-- navigation -->
                            <div class="gdlr-navigation-wrapper">

                               <?php

                               // navigation

                                   if( class_exists('gdlr_menu_walker') ){
                                       echo '<nav class="gdlr-navigation" id="gdlr-main-navigation" role="navigation">';
                                       wp_nav_menu( array(
                                           'theme_location'=>'primary',
                                           'container'=> '',
                                           'menu_class'=> 'sf-menu gdlr-main-menu',
                                           'walker'=> new gdlr_menu_walker()
                                       ) );
                                   }

                               ?>

                                <span class="gdlr-menu-search-button-sep">•</span>
                                <i class="fa fa-search icon-search gdlr-menu-search-button" id="gdlr-menu-search-button"></i>
                                <div class="gdlr-menu-search" id="gdlr-menu-search">
                                    <form method="get" id="searchform" action="http://www.99hotels.ch/demo/jungfrau/">
                                        <div class="search-text">
                                            <input type="text" value="Type Keywords" name="s" autocomplete="off" data-default="Type Keywords">
                                        </div>
                                        <input type="submit" value="">
                                        <div class="clear"></div>
                                    </form>
                                </div>
                                <div class="gdlr-navigation-gimmick" id="gdlr-navigation-gimmick" style="width: 40px; left: 644.641px; top: 88px; overflow: hidden;"></div><div class="clear"></div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </header>
