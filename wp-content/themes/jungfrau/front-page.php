<?php
//INCLUDE THE HEADER
get_header();
?>

<!-- ****************** BANNER ******************-->
<section id="content-section-homebanner">
    <?php get_template_part('include/element', 'banner'); ?>
</section>

<!-- ****************** BOOKING ******************-->
<section id="content-section-booking">
    <?php get_template_part('include/element', 'booking'); ?>
</section>

<!-- ****************** ROOM SLIDER ******************-->
<section id="content-section-room">
    <div class="gdlr-color-wrapper  gdlr-show-all no-skin"
         style="background-color: #ffffff; padding-top: 80px; padding-bottom: 10px; ">
        <div class="container">
            <div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-nav-container ">
                <div class="gdlr-item-title-head"><h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">
                        Unsere
                        Räume</h3>

                    <div class="gdlr-item-title-carousel"><i class="icon-angle-left gdlr-flex-prev"></i><i
                            class="icon-angle-right gdlr-flex-next"></i></div>
                    <div class="clear"></div>
                </div>
                <a class="gdlr-item-title-link" href="http://www.99hotels.ch/demo/jungfrau/_jungfrau/zimmer/">Alle
                    Zimmer anzeigen<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a>
            </div>
            <div class="room-item-wrapper type-modern">
                <div class="room-item-holder ">
                    <div class="gdlr-room-carousel-item gdlr-item">
                        <div class="flexslider" data-type="carousel" data-nav-container="room-item-wrapper"
                             data-columns="3">
                            <div class="clear"></div>
                            <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                <ul class="slides" style="width: 1200%; margin-left: -1140px;">
                                    <?php $query = new WP_Query(array('post_type' => 'rooms', 'posts_per_page' => 10)); ?>
                                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                                        <?php if (has_post_thumbnail() && !post_password_required()) : ?>

                                            <?php
                                            $image_id = get_post_thumbnail_id();
                                            $image_url = wp_get_attachment_image_src($image_id, 'medium', true);
                                            ?>

                                            <li class="gdlr-item gdlr-modern-room"
                                                style="width: 350px; float: left; display: block;">
                                                <div class="gdlr-room-thumbnail"><a
                                                        href="http://www.99hotels.ch/demo/jungfrau/room/doppelzimmer-standard/"><img
                                                            src="<?php echo $image_url[0]; ?>"
                                                            alt="" width="700" height="400" draggable="false"
                                                            style="transform: scale(1, 1);"></a></div>
                                                <h3 class="gdlr-room-title"><a
                                                        href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                                </h3><a
                                                    href="<?php the_permalink(); ?>"
                                                    class="gdlr-room-detail">Check Details<i
                                                        class="fa fa-long-arrow-right icon-long-arrow-right"></i></a>
                                            </li>
                                        <?php endif ?>
                                    <?php endwhile ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</section>

<?php
//INCLUDE THE FOOTER
get_footer();
?>