<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>

<header class="archive-header" style="background-image:url('<?php echo ht_category_banner(get_the_ID()); ?>')">
    <h1 class="archive-title">
        <?php the_title()?>
    </h1>
</header>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <h3>Custom Fields</h3>
    <p>
        events_date = <?php the_field('events_date'); ?><br>
        events_capacity = <?php the_field('events_capacity'); ?><br>
    </p>
</article>
<?php get_footer(); ?>