<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 */

get_header(); ?>

<header class="archive-header" style="background-image:url('<?php echo ht_category_banner(get_the_ID()); ?>')">
    <h1 class="archive-title">
        <?php the_title()?>
    </h1>
</header>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <h3>Custom Fields</h3>
    <p>
        staff_title = <?php the_field('staff_title'); ?><br>
        staff_email = <?php the_field('staff_email'); ?><br>
        staff_sykpe = <?php the_field('staff_sykpe'); ?><br>
        staff_facebook = <?php the_field('staff_facebook'); ?><br>
        staff_twitter = <?php the_field('staff_twitter'); ?><br>
        staff_google_plus = <?php the_field('staff_google_plus'); ?><br>
    </p>

</article>
<?php get_footer(); ?>