<div class="gdlr-color-wrapper  gdlr-show-all gdlr-skin-light-grey"
     style="background-color: #f5f5f5; padding-top: 70px; padding-bottom: 50px; ">
    <div class="container">
        <div class="gdlr-item-title-wrapper gdlr-item pos-center ">
            <div class="gdlr-item-title-head"><h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">
                    Einfach und schnell buchen</h3>

                <div class="clear"></div>
            </div>
        </div>
        <div class="gdlr-hotel-availability-wrapper" style="margin-bottom: 20px;">
            <form class="gdlr-hotel-availability gdlr-item" id="gdlr-hotel-availability" target="_blank"
                  method="post" action="http://www.booking.com/country/ch.de.html">
                <div class="gdlr-reservation-field gdlr-resv-datepicker"><span
                        class="gdlr-reservation-field-title">Anreise</span>

                    <div class="gdlr-datepicker-wrapper"><input type="text" id="gdlr-check-in"
                                                                class="gdlr-datepicker hasDatepicker"
                                                                data-dfm="d M yy" value="2016-03-20"><input
                            type="hidden" class="gdlr-datepicker-alt" name="gdlr-check-in" value="2016-03-20">
                    </div>
                </div>
                <div class="gdlr-reservation-field gdlr-resv-combobox"><span
                        class="gdlr-reservation-field-title">Nächte</span>

                    <div class="gdlr-combobox-wrapper"><select name="gdlr-night" id="gdlr-night">
                            <option value="0">0</option>
                            <option value="1" selected="">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select></div>
                </div>
                <div class="gdlr-reservation-field gdlr-resv-datepicker"><span
                        class="gdlr-reservation-field-title">Abreise</span>

                    <div class="gdlr-datepicker-wrapper">
                            <input type="text" id="gdlr-check-out"
                                                                class="gdlr-datepicker hasDatepicker"
                                                                data-dfm="d M yy" value="2016-03-21"><input
                            type="hidden" class="gdlr-datepicker-alt" name="gdlr-check-out" value="2016-03-21">
                    </div>
                </div>
                <div class="gdlr-reservation-field gdlr-resv-combobox"><span
                        class="gdlr-reservation-field-title">Erwachsene</span>

                    <div class="gdlr-combobox-wrapper"><select name="gdlr-adult-number[]">
                            <option value="0">0</option>
                            <option value="1" selected="">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select></div>
                </div>
                <div class="gdlr-reservation-field gdlr-resv-combobox"><span
                        class="gdlr-reservation-field-title">Kinder</span>

                    <div class="gdlr-combobox-wrapper"><select name="gdlr-children-number[]">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select></div>
                </div>
                <div class="gdlr-hotel-availability-submit"><input type="hidden" name="hotel_data"
                                                                   value="1"><input type="hidden"
                                                                                    name="gdlr-room-number"
                                                                                    value="1"><input
                        type="submit" class="gdlr-reservation-bar-button gdlr-button with-border"
                        value="Verfügbarkeit prüfen"></div>
                <div class="clear"></div>
            </form>
        </div>
        <div class="clear"></div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>