<?php
	/*	
	*	Goodlayers Menu Management File
	*	---------------------------------------------------------------------
	*	This file use to include a necessary script / function for the 
	* 	navigation area
	*	---------------------------------------------------------------------
	*/
	

	
	// creating the class for outputing the custom navigation menu
	if( !class_exists('gdlr_menu_walker') ){
		
		// from wp-includes/nav-menu-template.php file
		class gdlr_menu_walker extends Walker_Nav_Menu{		

			function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
				$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

				$class_names = $value = $data_column = $data_size = '';

				$classes = empty( $item->classes ) ? array() : (array) $item->classes;
				$classes[] = 'menu-item-' . $item->ID;

				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				if( $depth == 0 ){ 
					$class_names .= (empty($item->gdlr_mega_menu))? $class_names . ' gdlr-normal-menu': $class_names . ' gdlr-mega-menu';
				}else if( $depth == 1 && get_post_meta($item->menu_item_parent, '_gdlr_mega_menu_item', true) == 'mega_menu'){
					$data_size .= ' data-size="' . $item->gdlr_mega_section . '"';
					$data_column .= ' data-column="' . gdlr_get_column_class($item->gdlr_mega_section) . '"';
				}
				$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

				$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
				$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

				$output .= $indent . '<li ' . $id . $value . $class_names . $data_column . $data_size .'>';
				
				$atts = array();
				$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
				$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
				$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
				$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
				$atts['class']  = ! empty( $args->walker->has_children )? 'sf-with-ul-pre' : '';
				
				$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

				$attributes = '';
				foreach ( $atts as $attr => $value ) {
					if ( ! empty( $value ) ) {
						$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
						$attributes .= ' ' . $attr . '="' . $value . '"';
					}
				}

				$item_output = $args->before;
				$item_output .= '<a'. $attributes .'>';
				$item_output .= empty($item->gdlr_menu_icon)? '': '<i class="fa ' . gdlr_fa_class($item->gdlr_menu_icon) . '"></i>';
				$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
				$item_output .= '</a>';
				$item_output .= $args->after;
				
				$item_output .= (empty($item->gdlr_mega_menu) || $depth != 0)? '' : '<div class="sf-mega">'; // gdlr-modify
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}
			
			function end_el( &$output, $item, $depth = 0, $args = array() ) {
				$output .= (empty($item->gdlr_mega_menu) || $depth != 0)? '' : '</div>'; // gdlr-modify
				$output .= "</li>\n";
			}

		}
		
	}

