<?php
/**
 * Created by IntelliJ IDEA.
 * User: thomas
 * Date: 20.03.16
 * Time: 18:12
 */


    //GET THE ID OF RELATED BANNER
    $selectedBanner = get_field('bannerselect');
    $bannerID = $selectedBanner->ID;

    //GET THE BANNER POST QUERY
    // WP_Query arguments
    $args = array (
        'p'                      => $bannerID,
        'post_type'              => array( 'banner' ),
    );
    // The Query
    $query = new WP_Query( $args );

    ?>

    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
    <?php

    // GET ALL BANNER IMAGES
    $slides = get_field('banner_slides');

    if( $slides ): ?>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach( $slides as $slide ): ?>
                    <div class="swiper-slide" style="background-image:url(<?php echo $slide['banner_image']['sizes']['banner']; ?>)">
                        <h2 class="banner-title"><?php echo $slide['banner_title']; ?></h2>
                        <h3 class="banner-subtitle"><?php echo $slide['banner_subtitle']; ?></h3>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    <?php endif; ?>

<?php endwhile ?>