<?php
/*
Plugin Name: 99 Hotels
Plugin URI: http://www.99hotels.ch
Description: Adds Hotelspecific Functions to Wordpress
Version: 1.0
Author: Noel Bellón, Thomas Brandenburger
Author URI: http://www.99hotels.ch

*/

/**
 * Custom Post Types
 */
require_once('post-types.php');

/**
 * Shortcode for displaying Post Banner
 */
function ht_category_banner($post_id)
{
    $banner_img_url = '';
    $post_type = get_post_type($post_id);

    // Check if post or page has individual banner
    if (is_post_type_archive( $post_type ) == false && is_numeric(get_post_meta( $post_id, 'banner', true ))){
        $banner_id      = get_post_meta( $post_id, 'banner', true );
        $banner_img     = wp_get_attachment_image_src($banner_id, 'full');
        $banner_img_url	= $banner_img[0];
    }
    // Check if posttype is one of the custom muodules
    else{
        $module_banner_posts= new WP_Query( array( 'post_type'=> 'banner', 'posts_per_page' => -1 ) );

        while ( $module_banner_posts->have_posts() ){
            $module_banner_posts->the_post();

            $module_banner_post_id = get_the_id();
            $module_name      = get_post_meta( $module_banner_post_id, 'module', true );

            if ($post_type == $module_name){
                $banner_id      = get_post_meta( $module_banner_post_id, 'banner', true );
                $banner_img     = wp_get_attachment_image_src($banner_id, 'full');
                $banner_img_url	= $banner_img[0];
                break;
            }
        }
        wp_reset_query();
    }

    //$html_code = '<section id="internal-title" class="container" data-background="parallax" style="background-image:url('.esc_attr( $banner_img_url ).')"></section>';

    $html_code = $banner_img_url;

    return $html_code;
}
add_shortcode('ht-category-banner', 'ht_category_banner');

/**
 * Creates a Shortcut Link Menu for the Wordpress Admin
 */
function ad_create_shortlinks_menu() {
    global $wp_admin_bar;

    $menu_id = 'ad-shortlinks-menu';
    $wp_admin_bar->add_menu(array('id' => $menu_id, 'title' => __('Shortlinks'), 'href' => 'nav-menus.php'));
    $wp_admin_bar->add_menu(array('parent' => $menu_id, 'title' => __('Menüs'), 'id' => 'sl-menus', 'href' => 'nav-menus.php'));
    $wp_admin_bar->add_menu(array('parent' => $menu_id, 'title' => __('Theme Settings'), 'id' => 'sl-theme-settings', 'href' => 'themes.php?page=functions.php'));
    $wp_admin_bar->add_menu(array('parent' => $menu_id, 'title' => __('Theme Anpassen'), 'id' => 'sl-theme-customize', 'href' => 'customize.php?return=%2Fwp-admin%2Fnav-menus.php', 'meta' => array('target' => '_blank')));
}
add_action('admin_bar_menu', 'ad_create_shortlinks_menu', 2000);



/**
 * Offers a way to debug PHP Variables to the JavaScript console of the Browser
 */
function print_console( $data ) {

    $output = '<script type="text/javascript">console.info( \'Debug PHP in Console:\' );</script>';
    $output .= '<script type="text/javascript">console.log(' . json_encode( $data ) . ');</script>';

    echo $output;
}

/**
 * Get the current Template used for showing the content
 * Usage: add get_current_template() to the footer.php template
 */
add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}
function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}

