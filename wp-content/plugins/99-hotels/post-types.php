<?php
/**
 * Create Custom Post Types.
 */

// Prevent direct file call
defined( 'ABSPATH' ) or die( 'Forbidden Access' );

/**
 * Create Custom Post Type 'rooms'.
 */

function rooms_init() {
    $args = array(
        'label'             => 'Zimmer',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'rooms'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-admin-home',
        'supports'          => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'page-attributes'),
        'menu_position'     => 30
    );
    register_post_type( 'rooms', $args );
}
add_action( 'init', 'rooms_init' );

/**
 * Create Custom Post Type 'events'.
 */
add_action( 'init', 'events_init' );
function events_init() {
    $args = array(
        'label'             => 'Veranstaltungen',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'events'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-calendar-alt',
        'supports'          => array(
            'title',
            'editor',
            'thumbnail',
            'page-attributes'),
        'menu_position'     => 31
    );
    register_post_type( 'events', $args );
}

/**
 * Create Custom Post Type 'services'.
 */
add_action( 'init', 'services_init' );
function services_init() {
    $args = array(
        'label'             => 'Dienstleistungen',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'services'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-list-view',
        'supports'          => array(
            'title',
            'editor',
            'thumbnail',
            'page-attributes'),
        'menu_position'     => 32
    );
    register_post_type( 'services', $args );
}

/**
 * Create Custom Post Type 'history'.
 */
add_action( 'init', 'history_init' );
function history_init() {
    $args = array(
        'label'             => 'Geschichte',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'history'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-clock',
        'supports'          => array(
            'title',
            'editor',
            'thumbnail',
            'page-attributes'),
        'menu_position'     => 33
    );
    register_post_type( 'history', $args );
}

/**
 * Create Custom Post Type 'team'.
 */
add_action( 'init', 'team_init' );
function team_init() {
    $args = array(
        'label'             => 'Team',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'team'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-groups',
        'supports'          => array(
            'title',
            'editor',
            'thumbnail',
            'page-attributes'),
        'menu_position'     => 34
    );
    register_post_type( 'team', $args );
}

/**
 * Create Custom Post Type 'specials'.
 */
add_action( 'init', 'specials_init' );
function specials_init() {
    $args = array(
        'label'             => 'Angebote',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'specials'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-products',
        'supports'          => array(
            'title',
            'editor',
            'thumbnail',
            'page-attributes'),
        'menu_position'     => 35
    );
    register_post_type( 'specials', $args );
}

/**
 * Create Custom Post Type 'banner'.
 */
add_action( 'init', 'banner_init' );
function banner_init() {
    $args = array(
        'label'             => 'Banner',
        'public'            => true,
        'show_ui'           => true,
        'capability_type'   => 'post',
        'hierarchical'      => false,
        'rewrite'           => array('slug' => 'banner'),
        'query_var'         => true,
        'menu_icon'         => 'dashicons-format-image',
        'supports'          => array(
            'title',
            'editor',
            'thumbnail',
            'page-attributes'),
        'menu_position'     => 36
    );
    register_post_type( 'banner', $args );
}






